/// <reference path='../typings/index.d.ts' />

import { config } from './config';

module HW {
    'use strict';

    let deps: string[] = [
        'ui.router',

        'hw.main',
        'hw.guest-book'
    ];

    angular.module('hw', deps).config(config);
}

import './app/main/_module';
import './app/guest-book/_module.ts';
