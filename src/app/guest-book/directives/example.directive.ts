'use strict';

export class ExampleDirective {
    public scope = false;
    public restrict = 'A';


    public link = (scope: ng.IScope, elem: any) => {
        elem.html('I\'m message from directive. How are you? :) ');
    };

    public static DirectiveFn(): ExampleDirective {
        return new ExampleDirective();
    }
}


angular.module('hw.guest-book').directive('example', ExampleDirective.DirectiveFn);
