var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');

var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.common.js');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

const MODULE_NAME = process.env.MODULE_NAME || false;

module.exports = webpackMerge(commonConfig, {
    entry: {
        'vendor': './src/vendor.ts',
        'main':   './src/main.ts'
    },

    output: {
        path: './dist',
        filename: '[name].[hash].js',
        chunkFilename: '[id].[hash].chunk.js'
    },

    module: {
        loaders: [
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file?name=/assets/[name].[hash].[ext]'
            }
        ]
    },

    devtool: 'source-map',

    plugins: [
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.EnvironmentPlugin('NODE_ENV', ENV)
    ]
});
