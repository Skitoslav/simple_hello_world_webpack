config.$inject = ['$httpProvider', '$stateProvider', '$urlRouterProvider'];
export function config($httpProvider: ng.IHttpService,
                       $stateProvider: ng.ui.IStateProvider,
                       $urlRouterProvider: ng.ui.IUrlRouterProvider): any {
    'use strict';

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('root', {
            url: '/',
            template: '<main></main>'
        })
        .state('guestbook', {
            url: '/guest-book',
            template: '<guest-book></guest-book>'
        });


}
