'use strict';

export class GuestBookController {
    /*public static $inject = [
        '$state',
        '$uibModal',
        'Calendar',
        'toaster'
    ];*/

    public controllerScopedVariable: string = 'I\'m controller variable.';

    constructor() {
    }
}

angular.module('hw.guest-book').controller('GuestBookCtrl', GuestBookController);
