export class MainComponent implements ng.IComponentOptions {
    public template: string = `Hey! <i class="glyphicon glyphicon-thumbs-up"></i>
        How are you? Maybe you want to chat? Go to guest book :)
        <a ui-sref="guestbook"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>`;
}

angular.module('hw.main').component('main', new MainComponent());
