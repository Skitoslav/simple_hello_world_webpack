var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var path = require('path');

// Helper functions
var ROOT = path.resolve(__dirname, '..');

const MODULE_NAME = process.env.MODULE_NAME || false;


module.exports = {

    resolve: {
        extensions: ['', '.ts', '.js', '.css'],
        modulesDirectories: [ 'node_modules' ]
    },

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'awesome-typescript', //TODO change to awesome
                exclude: [/\.(spec|e2e)\.ts$/]
            },
            {
                test: /\.css$/,
                exclude: /\/app\//,
                loader: ExtractTextPlugin.extract('style', 'css', 'autoprefixer-loader?browsers=last 2 versions')
            },
            {
                test: /\.css$/,
                include: /\/app\//,
                loader: 'file?name=styles/[2].[hash].[ext]&regExp=src/(.*)/app/(.*).css!autoprefixer-loader?browsers=last 2 versions'
            },
            {
                test: /\.json$/,
                include: /\/app\//,
                loader: 'file-loader?name=json/[2]&regExp=src/(.*)/app/(.*).json'
            },
            {
                test: /\.html$/,
                include: /\/app\//,
                loader: 'file-loader?name=templates/[1]&regExp=src/app/(.*)'
            }
        ]
    },

    devtool: 'source-map',

    plugins: [
        // new ForkCheckerPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.DedupePlugin(),
        new ExtractTextPlugin('/styles/[name].[hash].css'),
        new CleanWebpackPlugin(['dist'], {
            root: ROOT,
            verbose: true,
            dry: false
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html'
        })
    ]
};
