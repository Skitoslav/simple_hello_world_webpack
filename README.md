# Simple Hello World on AngularJS 1.5.x with using webpack.

## Installation

````bash
$ npm install -g webpack webpack-dev-server typings
$ npm install
````

## Run developer web-server

````bash
$ npm run serve
````

## Build in developer mode

````bash
$ npm run build:dev
````

## Build in production mode

````bash
$ npm run build:prod
````
