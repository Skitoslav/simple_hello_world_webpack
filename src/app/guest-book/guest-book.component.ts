import { GuestBookController } from './contorllers/guest-book.controller';

export class GuestBookComponent implements ng.IComponentOptions {
    public templateUrl = require('./guest-book.component.html');
    public controller = GuestBookController;
    public controllerAs = 'guest';
}

angular.module('hw.main').component('guestBook', new GuestBookComponent());
